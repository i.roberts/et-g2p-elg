# ELG wrapper for Estonian Grapheme-to-Phoneme mapper

This is a simple wrapper for https://github.com/alumae/et-g2p to make it available as a web service compliant with the requirement of the [European Language Grid](https://www.european-language-grid.eu).

## Cloning this repo

This project includes the original `et-g2p` tool as a Git submodule.  When you clone this repo you should use `git clone --recurse-submodules` to fetch the original code along with the ELG wrapper.

## Building locally

To build and run the service locally, use

```
./gradlew shadowJar
```

This will create `build/libs/et-g2p-elg-0.1-all.jar` which is runnable with `java -jar`

## Docker image

The latest version of this tool, along with any tagged versions, are available as Docker images from the GitLab container registry

```
docker run --rm -it -p 8080:8080 registry.gitlab.com/i.roberts/et-g2p-elg:latest
```

The published images are multi-architecture and will run on amd64 or arm64 platforms.

To build an image locally you can run

```
./gradlew jibDockerBuild
```

For other options, see the [jib documentation](https://github.com/GoogleContainerTools/jib/tree/master/jib-gradle-plugin).