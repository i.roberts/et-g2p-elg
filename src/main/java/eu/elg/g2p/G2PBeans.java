package eu.elg.g2p;

import ee.ioc.phon.g2p.G2P;
import io.micronaut.context.annotation.Bean;
import io.micronaut.context.annotation.Factory;

import java.util.HashMap;

@Factory
public class G2PBeans {

  @Bean
  public G2P g2p() {
    return new G2P(new HashMap<>());
  }
}
