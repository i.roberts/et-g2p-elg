package eu.elg.g2p;

import ee.ioc.phon.g2p.G2P;
import ee.ioc.phon.g2p.TooComplexWordException;
import eu.elg.ltservice.LTService;
import eu.elg.model.AnnotationObject;
import eu.elg.model.requests.TextRequest;
import eu.elg.model.responses.AnnotationsResponse;
import eu.elg.model.util.TextOffsetsHelper;
import io.micronaut.http.annotation.Controller;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@Controller("/process")
public class ProcessController extends LTService<TextRequest, LTService.Context> {

  private static final Pattern WORD_PATTERN = Pattern.compile("\\S+");

  private final G2P g2p;

  public ProcessController(G2P g2p) {
    this.g2p = g2p;
  }

  @Override
  protected AnnotationsResponse handleSync(TextRequest request, Context ctx) throws Exception {
    String content = request.getContent();
    Matcher m = WORD_PATTERN.matcher(content);
    TextOffsetsHelper h = new TextOffsetsHelper(content);
    List<AnnotationObject> words = new ArrayList<>();

    while(m.find()) {
      try {
        var pronounciations = g2p.graphemes2Phonemes(m.group());
        words.add(h.annotationWithOffsets(m.start(), m.end()).withFeature("analysis",
                pronounciations.stream().map((a) -> String.join(" ", a)).collect(Collectors.toList())));
      } catch(TooComplexWordException e) {
        // ignore this word
      }
    }

    return new AnnotationsResponse().withAnnotations("word", words);
  }
}
